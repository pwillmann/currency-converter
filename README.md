# Currency Converter Android

## Design Decisions:
- Dagger -> not needed for a project this size
- Android Jetpack Libraries
- Fragment with navigation library
- Dark mode (why not)
- Added Debug code to "main" flavor for simplicity (should be only for "debug" flavor)

## Possible Improvements 
This is just a List of things that I would improve with more time:
- Use MvRX instead of plain android MVVM to reduce the number of Subjects and improve the Reactivity
- Use API's to get the currencies Flags and full name
- Use Room as Single Source of Truth for better offline support
- "Active" item is currently not moved to the top immediately because the list is only updated once per second when new data arrives -> rearrange items after click too
- Add Unit +  UI Tests

## Time Breakdown
- 1h: Project Setup (haven't started a fresh project in a while and wanted to use Kotlin Gradle DSL + buildSrc + detekt)
- 0.5h: Debugging + Dagger Setup (Flipper)
- 0.5h: Basic styling Setup
- 1h: Moshi + Retrofit setup and getting the rates from the API
- 4h: Fragment + ViewModel + Recyclerview Setup + First Version implementation
- 2h: Improved the Recyclerview
- 0.5h: Offline Support (basic Error handling + Snackbar + Progress)
- 0.5h: Dark Mode (because why not)

Total: 9h

