import com.android.build.gradle.BaseExtension

// Top-level build file where you can add configuration options common to all sub-projects/modules
buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.0.0-beta01")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${com.pwillmann.android.Versions.kotlin}")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

plugins {
    id("com.diffplug.gradle.spotless") version "3.27.0"
    id("io.gitlab.arturbosch.detekt") version "1.6.0"
}



allprojects {
    repositories {
        google()
        jcenter()
    }
}

subprojects {
    project.pluginManager.apply("com.diffplug.gradle.spotless")
    project.pluginManager.apply("io.gitlab.arturbosch.detekt")

    spotless {
        java {
            // This is required otherwise the code in android modules isn"t picked up by spotless.
            target("**/*.java")
            trimTrailingWhitespace()
            removeUnusedImports()
            googleJavaFormat()
        }

        kotlin {
            target("**/*.kt")
            ktlint("0.36.0").userData(
                hashMapOf(
                    "indent_size" to "4",
                    "android" to "true",
                    "max_line_length" to "200"
                )
            )
        }

        kotlinGradle {
            target("**/*.gradle.kts")
            ktlint("0.36.0").userData(
                hashMapOf(
                    "indent_size" to "4",
                    "android" to "true",
                    "max_line_length" to "200"
                )
            )
        }

        format("misc") {
            target("**/.gitignore", "**/*.gradle", "**/*.md", "**/*.sh", "**/*.yml")
            trimTrailingWhitespace()
            endWithNewline()
        }
    }

    detekt {
        toolVersion = "1.6.0"
        config = files("$rootDir/default-detekt-config.yml")
        reports {
            xml.destination = file("${project.projectDir}/build/reports/detekt.xml")
            html.destination = file("${project.projectDir}/build/reports/detekt.html")
        }
        parallel = true
        baseline = file("detekt-baseline.xml")
    }

    project.plugins.whenPluginAdded {
        when (this) {
            is com.android.build.gradle.AppPlugin, is com.android.build.gradle.LibraryPlugin -> {
                the<BaseExtension>().apply {
                    sourceSets {
                        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
                        getByName("debug").java.srcDirs("src/debug/kotlin")
                        getByName("main").java.srcDirs("src/main/kotlin")
                        getByName("test").java.srcDirs("src/test/kotlin")
                    }
                    compileOptions {
                        targetCompatibility = JavaVersion.VERSION_1_8
                        sourceCompatibility = JavaVersion.VERSION_1_8
                    }

                    lintOptions.lintConfig = rootProject.file("lint.xml")

                }
            }

        }
    }

}
