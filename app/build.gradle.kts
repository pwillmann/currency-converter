import com.pwillmann.android.AppDeps
import com.pwillmann.android.DebugDeps
import com.pwillmann.android.Versions

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(Versions.compileSdk)

    defaultConfig {
        applicationId = "com.pwillmann.revolutcurrencies"
        minSdkVersion(Versions.minSdk)
        targetSdkVersion(Versions.targetSdk)
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        getByName("debug") {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            isDebuggable = true
            isZipAlignEnabled = false
            isMinifyEnabled = false
        }
        getByName("release") {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            isDebuggable = false
            isZipAlignEnabled = true
            isMinifyEnabled = true
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(AppDeps.kotlinStdlib)

    implementation(AppDeps.AndroidX.core)
    implementation(AppDeps.AndroidX.appCompat)
    implementation(AppDeps.AndroidX.constraintLayout)
    implementation(AppDeps.AndroidX.fragment)
    implementation(AppDeps.AndroidX.material)
    implementation(AppDeps.AndroidX.navFragment)
    implementation(AppDeps.AndroidX.navUi)
    implementation(AppDeps.AndroidX.viewModel)
    kapt(AppDeps.AndroidX.lifecycleCompiler)

    implementation(AppDeps.Glide.core)

    kapt(AppDeps.Dagger.compiler)
    kapt(AppDeps.Dagger.androidProcessor)
    implementation(AppDeps.Dagger.runtime)
    implementation(AppDeps.Dagger.support)

    implementation(AppDeps.Rx.core)
    implementation(AppDeps.Rx.android)

    implementation(AppDeps.timber)
    implementation(AppDeps.Retrofit.core)
    implementation(AppDeps.Retrofit.moshi)
    implementation(AppDeps.Retrofit.rxjava)
    implementation(AppDeps.Moshi.core)
    implementation(AppDeps.Moshi.kotlin)
    kapt(AppDeps.Moshi.codeGen)

    // This should be debugImplementation (for simplicities sake its in the release app)
    implementation(DebugDeps.Flipper.core)
    implementation(DebugDeps.Flipper.network)
    implementation(DebugDeps.soloader)
}
