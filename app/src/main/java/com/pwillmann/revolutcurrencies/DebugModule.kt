package com.pwillmann.revolutcurrencies

import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DebugModule {

    @Provides
    @Singleton
    fun provideFlipperNetworkPlugin(): NetworkFlipperPlugin = NetworkFlipperPlugin()
}
