package com.pwillmann.revolutcurrencies

import android.content.res.Configuration
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pwillmann.revolutcurrencies.extensions.isDarkTheme
import dagger.android.AndroidInjection

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        applySystemUiFlags(resources.configuration.isDarkTheme())
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) applySystemUiFlags(resources.configuration.isDarkTheme())
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        applySystemUiFlags(newConfig.isDarkTheme())
    }

    private fun applySystemUiFlags(isDarkTheme: Boolean) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor =
                if (isDarkTheme) Color.TRANSPARENT else Color.argb(40, 0, 0, 0)
            window.statusBarColor = if (isDarkTheme) Color.TRANSPARENT else Color.argb(40, 0, 0, 0)
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            window.navigationBarColor =
                if (isDarkTheme) Color.TRANSPARENT else Color.argb(40, 0, 0, 0)
        }
    }
}
