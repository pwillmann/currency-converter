package com.pwillmann.revolutcurrencies.extensions

import com.squareup.moshi.JsonReader

inline fun JsonReader.readObject(body: () -> Unit) {
    beginObject()
    while (hasNext()) {
        body()
    }
    endObject()
}
