package com.pwillmann.revolutcurrencies.extensions

import android.content.res.Configuration

fun Configuration.isDarkTheme() =
    uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
