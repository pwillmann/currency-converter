package com.pwillmann.revolutcurrencies.inject

import androidx.lifecycle.ViewModel
import com.pwillmann.revolutcurrencies.MainActivity
import com.pwillmann.revolutcurrencies.ui.rates.RatesFragment
import com.pwillmann.revolutcurrencies.ui.rates.RatesViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class FeatureBuilder {
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun provideRatesFragment(): RatesFragment

    @Binds
    @IntoMap
    @ViewModelKey(RatesViewModel::class)
    internal abstract fun bindRatesViewModel(userViewModel: RatesViewModel): ViewModel
}
