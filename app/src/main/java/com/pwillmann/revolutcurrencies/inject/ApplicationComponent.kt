package com.pwillmann.revolutcurrencies.inject

import android.content.Context
import com.pwillmann.revolutcurrencies.DebugModule
import com.pwillmann.revolutcurrencies.RevolutApplication
import com.pwillmann.revolutcurrencies.data.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        DataModule::class,
        DebugModule::class,
        FeatureBuilder::class,
        ViewModelFactoryModule::class]
)
interface ApplicationComponent : AndroidInjector<RevolutApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): ApplicationComponent
    }
}
