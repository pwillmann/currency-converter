package com.pwillmann.revolutcurrencies.ui.rates

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pwillmann.revolutcurrencies.R
import com.pwillmann.revolutcurrencies.data.model.Currency
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import java.text.DecimalFormat
import kotlinx.android.synthetic.main.item_currency.view.code
import kotlinx.android.synthetic.main.item_currency.view.flag
import kotlinx.android.synthetic.main.item_currency.view.input
import kotlinx.android.synthetic.main.item_currency.view.name

class RatesListAdapter(private val viewModel: RatesViewModel) :
    ListAdapter<Currency, RatesListAdapter.ViewHolder>(RateDiffCallback()) {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            inflater.inflate(R.layout.item_currency, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.setViewModel(viewModel)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    override fun getItemId(position: Int): Long {
        val item = getItem(position)
        return item.name.hashCode().toLong()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val disposables = CompositeDisposable()
        private var currency: Currency? = null
        private var sharedViewModel: RatesViewModel? = null
        private var isActive = false

        companion object {
            private const val DECIMAL_FORMAT = "0.000"
        }

        init {
            itemView.input.addTextChangedListener(onTextChanged = { text: CharSequence?, _: Int, _: Int, _: Int ->
                val number = if (text.toString().isNullOrEmpty()) {
                    0.0
                } else {
                    val percentageFormat = DecimalFormat(DECIMAL_FORMAT)
                    percentageFormat.parse(text.toString()).toDouble()
                }
                if (sharedViewModel?.activeCurrency == currency?.code) {
                    sharedViewModel?.activeRate = number
                } else {
                    isActive = false
                }
            })
            itemView.input.setOnTouchListener { _, motionEvent ->
                if (MotionEvent.ACTION_UP == motionEvent.action && currency?.code != null) {
                    sharedViewModel?.activeCurrency = currency?.code ?: ""
                    isActive = true
                }
                return@setOnTouchListener false
            }
        }

        fun bind(
            currency: Currency
        ) {
            itemView.code.text = currency.code
            itemView.name.text = currency.name
            Glide.with(itemView.context)
                .load(currency.resId)
                .into(itemView.flag)
            this.currency = currency
        }

        fun setViewModel(viewModel: RatesViewModel) {
            this.sharedViewModel = viewModel
            if (sharedViewModel == null)
                return
            disposables
                .add(
                    Observable.combineLatest(
                        sharedViewModel!!.activeValue,
                        sharedViewModel!!.rates,
                        BiFunction { value: Double, rates: HashMap<String, Double> ->
                            return@BiFunction value to rates
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            if (currency == null) {
                                return@subscribe
                            }
                            val newRate = it.second[currency!!.code] ?: 1.0
                            val baseRate = it.second[sharedViewModel?.activeCurrency] ?: 1.0
                            currency = currency!!.copy(rate = newRate)
                            val newValue = calcValue(
                                targetRate = newRate,
                                baseRate = baseRate,
                                baseValue = it.first
                            )
                            if (currency?.code != sharedViewModel?.activeCurrency || isActive.not()) {
                                val percentageFormat = DecimalFormat(DECIMAL_FORMAT)
                                itemView.input.setText(percentageFormat.format(newValue))
                            }
                        })
        }

        fun unbind() {
            disposables.clear()
            sharedViewModel = null
        }

        private fun calcValue(
            targetRate: Double,
            baseRate: Double,
            baseValue: Double
        ): Double {
            return (targetRate / baseRate) * baseValue
        }
    }

    class RateDiffCallback : DiffUtil.ItemCallback<Currency>() {
        override fun areItemsTheSame(
            oldItem: Currency,
            newItem: Currency
        ): Boolean {
            return oldItem.code == newItem.code
        }

        override fun areContentsTheSame(
            oldItem: Currency,
            newItem: Currency
        ): Boolean {
            return oldItem.code == oldItem.code
        }
    }
}
