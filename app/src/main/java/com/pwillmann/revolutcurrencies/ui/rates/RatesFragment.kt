package com.pwillmann.revolutcurrencies.ui.rates

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.snackbar.Snackbar
import com.pwillmann.revolutcurrencies.R
import com.pwillmann.revolutcurrencies.extensions.isDarkTheme
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import kotlinx.android.synthetic.main.fragment_rates.progress
import kotlinx.android.synthetic.main.fragment_rates.rates_list
import kotlinx.android.synthetic.main.fragment_rates.toolbar

class RatesFragment : Fragment(R.layout.fragment_rates) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val ratesViewModel: RatesViewModel by viewModels { viewModelFactory }
    private val ratesListAdapter: RatesListAdapter by lazy { RatesListAdapter(ratesViewModel) }

    private val disposables = CompositeDisposable()
    private var errorSnackbar: Snackbar? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupRecyclerView()
        observeViewModel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }

    private fun observeViewModel() {
        disposables.add(
            ratesViewModel.currencies
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.isEmpty().not()) {
                        progress.isVisible = false
                        errorSnackbar?.dismiss()
                    }
                    it
                }
                .subscribe(ratesListAdapter::submitList)
        )
        disposables.add(
            ratesViewModel.errors
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (errorSnackbar == null || errorSnackbar!!.isShown.not()) {
                        errorSnackbar = Snackbar.make(
                            rates_list,
                            R.string.rates_error_message_internet,
                            Snackbar.LENGTH_INDEFINITE
                        )

                        errorSnackbar!!.setBackgroundTint(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.color_primary
                            )
                        )
                        errorSnackbar!!.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.color_on_primary
                            )
                        )
                    }
                    errorSnackbar!!.setText(R.string.rates_error_message_internet)
                    errorSnackbar!!.show()
                }
        )
    }

    private fun toggleDarkMode() {
        if (resources.configuration.isDarkTheme()) {
            (activity as AppCompatActivity).delegate.localNightMode =
                AppCompatDelegate.MODE_NIGHT_NO
            toolbar.menu.findItem(R.id.action_toggle_darkmode).setIcon(R.drawable.ic_night)
        } else {
            (activity as AppCompatActivity).delegate.localNightMode =
                AppCompatDelegate.MODE_NIGHT_YES
            toolbar.menu.findItem(R.id.action_toggle_darkmode).setIcon(R.drawable.ic_day)
        }
    }

    private fun setupToolbar() {
        toolbar.inflateMenu(R.menu.menu_main)
        toolbar.setOnMenuItemClickListener {
            return@setOnMenuItemClickListener when (it.itemId) {
                R.id.action_toggle_darkmode -> {
                    toggleDarkMode()
                    true
                }
                else -> false
            }
        }
        if (resources.configuration.isDarkTheme()) {
            toolbar.menu.findItem(R.id.action_toggle_darkmode).setIcon(R.drawable.ic_day)
        } else {
            toolbar.menu.findItem(R.id.action_toggle_darkmode).setIcon(R.drawable.ic_night)
        }
    }

    private fun setupRecyclerView() {
        rates_list.adapter = ratesListAdapter
        rates_list.setHasFixedSize(true)
        (rates_list.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
    }
}
