package com.pwillmann.revolutcurrencies.ui.rates

import androidx.lifecycle.ViewModel
import com.pwillmann.revolutcurrencies.data.local.LocalDataManager
import com.pwillmann.revolutcurrencies.data.model.Currency
import com.pwillmann.revolutcurrencies.data.remote.CurrenciesJson
import com.pwillmann.revolutcurrencies.data.remote.RevolutApi
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import retrofit2.HttpException
import timber.log.Timber

class RatesViewModel @Inject constructor(
    private val revolutApi: RevolutApi,
    private val localDataManager: LocalDataManager
) : ViewModel() {
    private val disposables = CompositeDisposable()

    private val ratesMapSubject = BehaviorSubject.create<HashMap<String, Double>>()
    private val currenciesSubject = BehaviorSubject.create<List<Currency>>()
    private val activeValueSubject = BehaviorSubject.create<Double>()
    private val errorSubject = PublishSubject.create<Throwable>()

    val rates = ratesMapSubject.distinctUntilChanged()
    val currencies = currenciesSubject.distinctUntilChanged { old, new ->
        for ((index, value) in old.withIndex()) {
            if (value.code != new[index].code ||
                value.name != new[index].name
            ) {
                return@distinctUntilChanged false
            }
        }
        return@distinctUntilChanged true
    }
    val activeValue = activeValueSubject.distinctUntilChanged()
    val errors = errorSubject.distinctUntilChanged()

    var activeCurrency: String = "USD"
        set(value) {
            field = value
        }

    var activeRate: Double = 1.0
        set(value) {
            field = value
            activeValueSubject.onNext(value)
        }

    init {
        activeCurrency = "USD"
        activeRate = 1.0
        Observable.interval(1, TimeUnit.SECONDS)
            .flatMap {
                revolutApi.getRates(activeCurrency)
                    .toObservable()
                    .onErrorReturn {
                        // Ignore all connection / internet problems and show a hint for the user
                        if (isInternetException(it)) {
                            errorSubject.onNext(it)
                            return@onErrorReturn CurrenciesJson.EMPTY
                        } else {
                            Timber.e(it)
                            throw it
                        }
                    }
            }
            .subscribeOn(Schedulers.io())
            .filter { it != CurrenciesJson.EMPTY }
            .subscribe { currenciesJson ->
                updateRates(currenciesJson.currencies)
            }.disposeOnClear()
    }

    private fun isInternetException(throwable: Throwable): Boolean {
        return when (throwable) {
            is UnknownHostException -> true
            is HttpException -> true
            else -> false
        }
    }

    private fun updateRates(baseCurrencies: List<Currency>) {
        val names = localDataManager.currenciesNames
        var currencies = baseCurrencies
        if (currencies.find { it.code == activeCurrency } == null) {
            currencies =
                currencies.plus(Currency(code = activeCurrency, rate = 1.0))
        }
        currencies = sortCurrencies(currencies, activeCurrency)
        val ratesMap = hashMapOf<String, Double>()
        currencies.forEach {
            ratesMap[it.code] = it.rate
            it.name = names[it.code] ?: it.code
            it.resId = localDataManager.getFlagImageIdByCurrencyCode(it.code)
        }
        ratesMapSubject.onNext(ratesMap)
        currenciesSubject.onNext(currencies)
    }

    private fun sortCurrencies(
        currencies: List<Currency>,
        activeCurrencyCode: String
    ): List<Currency> {
        return currencies.sortedWith(compareBy<Currency> { it.code != activeCurrencyCode }.thenBy { it.code })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    private fun Disposable.disposeOnClear(): Disposable {
        disposables.add(this)
        return this
    }
}
