package com.pwillmann.revolutcurrencies.data.local

data class CurrencyNamesJson(val currencyNamesMap: HashMap<String, String>)
