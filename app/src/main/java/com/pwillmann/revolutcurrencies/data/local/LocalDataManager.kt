package com.pwillmann.revolutcurrencies.data.local

import android.content.Context
import androidx.annotation.DrawableRes
import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataManager @Inject constructor(
    private val context: Context,
    private val moshi: Moshi
) {
    val currenciesNames: HashMap<String, String> by lazy {
        val file = "currencies_names.json"
        val json = context.assets.open(file).bufferedReader().use { it.readText() }

        val currencyNamesJson: CurrencyNamesJson? =
            moshi.adapter(CurrencyNamesJson::class.java).fromJson(json)

        return@lazy currencyNamesJson!!.currencyNamesMap
    }

    @DrawableRes
    fun getFlagImageIdByCurrencyCode(code: String): Int? {
        return context.resources.getIdentifier(code.toLowerCase(), "drawable", context.packageName)
    }
}
