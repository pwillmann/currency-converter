package com.pwillmann.revolutcurrencies.data.local

import com.pwillmann.revolutcurrencies.extensions.readObject
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.ToJson
import java.text.ParseException
import org.json.JSONObject

object CurrencyNamesJsonAdapter {
    @ToJson
    internal fun toJson(rates: CurrencyNamesJson): String {
        val jsonObject = JSONObject()
        for (namesMap in rates.currencyNamesMap) {
            jsonObject.put(namesMap.key, namesMap.value)
        }
        return jsonObject.toString()
    }

    @FromJson
    @Throws(ParseException::class)
    internal fun fromJson(jsonReader: JsonReader): CurrencyNamesJson {
        val currencyNames = hashMapOf<String, String>()
        jsonReader.readObject {
            while (jsonReader.hasNext()) {
                currencyNames[jsonReader.nextName()] = jsonReader.nextString()
            }
        }
        return CurrencyNamesJson(currencyNames)
    }
}
