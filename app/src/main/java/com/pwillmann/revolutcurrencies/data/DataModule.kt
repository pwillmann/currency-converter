package com.pwillmann.revolutcurrencies.data

import com.pwillmann.revolutcurrencies.data.local.CurrencyNamesJsonAdapter
import com.pwillmann.revolutcurrencies.data.remote.CurrenciesJsonAdapter
import com.pwillmann.revolutcurrencies.data.remote.RemoteModule
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RemoteModule::class])
object DataModule {

    @Provides
    @Singleton
    fun moshi(): Moshi = Moshi.Builder()
        .add(CurrenciesJsonAdapter)
        .add(CurrencyNamesJsonAdapter)
        .add(KotlinJsonAdapterFactory())
        .build()
}
