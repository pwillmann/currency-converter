package com.pwillmann.revolutcurrencies.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("android/latest")
    fun getRates(@Query("base") base: String = "EUR"): Single<CurrenciesJson>
}
