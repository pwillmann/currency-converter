package com.pwillmann.revolutcurrencies.data.remote

import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
object RemoteModule {

    @Provides
    @Singleton
    fun okHttpClient(flipperNetworkFlipperPlugin: NetworkFlipperPlugin): OkHttpClient {
        val builder = OkHttpClient.Builder()
        return builder.readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addNetworkInterceptor(FlipperOkhttpInterceptor(flipperNetworkFlipperPlugin))
            .build()
    }

    @Provides
    @Singleton
    fun retrofitBuilder(moshi: Moshi): Retrofit.Builder = Retrofit.Builder()
        .baseUrl("https://hiring.revolut.codes/api/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(moshi))

    @Provides
    @Singleton
    fun retrofit(builder: Retrofit.Builder, okHttpClient: OkHttpClient): Retrofit = builder
        .client(okHttpClient)
        .build()

    @JvmStatic
    @Provides
    @Singleton
    fun revolutApi(retrofit: Retrofit): RevolutApi {
        return retrofit.create(RevolutApi::class.java)
    }
}
