package com.pwillmann.revolutcurrencies.data.remote

import com.pwillmann.revolutcurrencies.data.model.Currency

data class CurrenciesJson(val baseCurrency: String = "EUR", val currencies: List<Currency>) {
    companion object {
        val EMPTY = CurrenciesJson(
            baseCurrency = "",
            currencies = listOf()
        )
    }
}
