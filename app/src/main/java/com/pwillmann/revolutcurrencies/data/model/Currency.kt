package com.pwillmann.revolutcurrencies.data.model

import androidx.annotation.DrawableRes

data class Currency(
    var name: String = "",
    val code: String = "",
    val rate: Double = 0.0,
    @DrawableRes var resId: Int? = null
)
