package com.pwillmann.revolutcurrencies.data.remote

import com.pwillmann.revolutcurrencies.data.model.Currency
import com.pwillmann.revolutcurrencies.extensions.readObject
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.ToJson
import java.text.ParseException
import org.json.JSONObject

object CurrenciesJsonAdapter {
    @ToJson
    internal fun toJson(currencyJson: CurrenciesJson): String {
        val wrapperObject = JSONObject()
        wrapperObject.put("baseCurrency", currencyJson.baseCurrency)
        val ratesObject = JSONObject()
        for (currency in currencyJson.currencies) {
            ratesObject.put(currency.code, currency.rate)
        }
        wrapperObject.put("rates", ratesObject)
        return wrapperObject.toString()
    }

    @FromJson
    @Throws(ParseException::class)
    internal fun fromJson(jsonReader: JsonReader): CurrenciesJson {
        var baseCurrency = ""
        val currencies = mutableListOf<Currency>()
        jsonReader.readObject {
            while (jsonReader.hasNext()) {
                when (jsonReader.nextName()) {
                    "baseCurrency" -> baseCurrency = jsonReader.nextString()
                    "rates" -> {
                        jsonReader.readObject {
                            while (jsonReader.hasNext()) {
                                currencies.add(
                                    Currency(
                                        code = jsonReader.nextName(),
                                        rate = jsonReader.nextDouble()
                                    )
                                )
                            }
                        }
                    }
                    else -> jsonReader.skipValue()
                }
            }
        }
        return CurrenciesJson(
            baseCurrency = baseCurrency,
            currencies = currencies
        )
    }
}
