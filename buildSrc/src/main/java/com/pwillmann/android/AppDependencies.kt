@file:Suppress("unused")

package com.pwillmann.android

object AppDeps {
    object LeakCanary {
        const val real = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanary}"
        const val instrumentation =
            "com.squareup.leakcanary:leakcanary-android-instrumentation:${Versions.leakCanary}"
        const val leaksentry = "com.squareup.leakcanary:leaksentry:${Versions.leakCanary}"
    }

    object Moshi {
        const val core = "com.squareup.moshi:moshi:${Versions.moshi}"
        const val adapters = "com.squareup.moshi:moshi-adapters:${Versions.moshi}"
        const val codeGen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
        const val kotlin = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    }

    object MvRx {
        const val core = "com.airbnb.android:mvrx:${Versions.mvrx}"
        const val testing = "com.airbnb.android:mvrx-testing:${Versions.mvrx}"
    }

    object Okhttp {
        const val mockwebserver = "com.squareup.okhttp3:mockwebserver:${Versions.okhttp}"
        const val logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    }

    object Retrofit {
        const val core = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
        const val mock = "com.squareup.retrofit2:retrofit-mock:${Versions.retrofit}"
        const val moshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
        const val rxjava = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    }

    object Rx {
        const val core = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
        const val android = "io.reactivex.rxjava2:rxandroid:2.0.2"
        const val binding = "com.jakewharton.rxbinding2:rxbinding:2.2.0"
        const val bindingKotlin = "com.jakewharton.rxbinding2:rxbinding-kotlin:2.1.1"
    }

    object AndroidX {
        const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
        const val material = "com.google.android.material:material:${Versions.material}"
        const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
        const val core = "androidx.core:core-ktx:${Versions.androidxCore}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
        const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"
        const val navUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
        const val navFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
        const val navTesting = "androidx.navigation:navigation-testing-ktx:${Versions.navigation}"
        const val navSafeArgsPlugin =
            "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
        const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycle}"
    }

    object Glide {
        const val core = "com.github.bumptech.glide:glide:${Versions.glide}"
        const val compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    }

    object Dagger {
        const val runtime = "com.google.dagger:dagger-android:${Versions.dagger}"
        const val support = "com.google.dagger:dagger-android-support:${Versions.dagger}"
        const val compiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
        const val androidProcessor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    }


    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
}

object TestingDeps {
    const val junit = "junit:junit:4.12"
    const val mockito = "org.mockito:mockito-core:2.23.4"
    const val mockitoInline = "org.mockito:mockito-inline:2.23.4"
    const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.0.0"
    const val truth = "com.google.truth:truth:1.0-rc2"
}

object DebugDeps {
    object Flipper {
        const val network = "com.facebook.flipper:flipper-network-plugin:${Versions.flipper}"
        const val core = "com.facebook.flipper:flipper:${Versions.flipper}"
    }

    const val soloader = "com.facebook.soloader:soloader:0.8.2"
}

object Versions {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
    const val kotlin = "1.3.70"
    const val timber = "4.7.1"
    const val flipper = "0.33.1"

    // androidX versions
    const val appCompat = "1.1.0"
    const val fragment = "1.2.2"
    const val constraintLayout = "2.0.0-beta4"
    const val androidxCore = "1.2.0"
    const val material = "1.2.0-alpha05"
    const val lifecycle = "2.3.0-alpha01"
    const val navigation = "2.3.0-alpha03"
    const val recyclerView = "1.1.0"
    const val room = "2.2.4"

    const val okhttp = "3.12.0"
    const val retrofit = "2.6.2"
    const val espresso = "3.1.0"
    const val moshi = "1.8.0"
    const val leakCanary = "2.0-beta-3"
    const val rxjava = "2.2.13"
    const val mvrx = "1.3.0"
    const val glide = "4.9.0"
    const val dagger = "2.26"
}
