plugins {
    `kotlin-dsl`
    // update to higher version when we upgrade gradle wrapper to at least 5.0 (should match kotlin version in AppDeps)
    kotlin("jvm") version "1.3.70"
}

repositories {
    jcenter()
}
